//Option Variables
var monitorFramerate = 144;

//--------------------------------------------------------------------------------------------------------------------------------------

//Process management
//THIS IS VERY IMPORTANT

function Process(processesArray, command, priority, name){
  this.command = command;
  this.PID = processesArray.length;
  this.processName = name;
	this.execRatio = 1;
  this.cycleCount = 0;
  this.frameTime = 0;
  this.prioritySum = 0;
  
	//Priority control
  if(priority !== undefined){
    this.priority = priority;
    
    //Convert priority for scheduler
    if(this.priority-1 < 0){
      this.convertedPriority = abs(priority);
    }else{
      this.convertedPriority = 1/priority;
    }
  }else{
    this.priority = 1;
    this.convertedPriority = 1;
  }
}

Process.prototype.update = function(){
  if(!this.suspend){
    this.cycleCount++;
	  if(this.cycleCount > this.execRatio){
	    
	    var timeBefore = millis();
	    this.command();
	    this.frameTime = millis() - timeBefore;
	     
	    this.cycleCount -= this.execRatio;
	  }
  }
};

Process.prototype.getInfo = function(pretty){
	var attributes = [this.command, this.name, this.PID, this.priority, this.frameTime, this.execRatio];
	if(pretty){
		attributes[0] = "Command: " + attributes[0].toString();
		attributes[1] = "Name: " + attributes[1];
		attributes[2] = "PID: " + attributes[2];
		attributes[3] = "Priority: " + attributes[3];
		attributes[4] = "Frame Time: " + attributes[4];
		attributes[5] = "Exec Ratio: " + attributes[5];
	}
	return attributes;
};

//Process scheduler
var targetLatency = 1000/monitorFramerate;

Process.prototype.schedule = function(prioritySum){
  if(this.priority !== 0){
    let executionRatio = this.frameTime / ((targetLatency / prioritySum) * this.convertedPriority);
    // R = l / ((t/P) * (1/p))
    if(executionRatio < 1){
      this.execRatio = 1;
    }else{
      this.execRatio = executionRatio;
    }
  }
}



//Process manager
var processes = [];

function createProcess(command,priority,name,processesArray){
  let currentProcessesArray = processes;
  if(processesArray){
    currentProcessesArray = processesArray;
  }
	currentProcessesArray.push(new Process(currentProcessesArray,command,priority,name));
  currentProcessesArray[0].prioritySum += currentProcessesArray[currentProcessesArray.length-1].convertedPriority;
}

function kill(PID){
	for(var i in processes){
    if(processes[i].PID === PID){
      processes.splice(i,1);
      print("Process " + PID + " killed");
    }
  }
}

function info(PID, pretty){
	return processes[PID].getInfo(pretty);
}

function find(processName){
  var foundProcesses = [];
  for(var i in processes){
    var currentProcess = processes[i];
    if(processName === currentProcess.processName){
      foundProcesses.push(currentProcess.PID);
    }
  }
  return foundProcesses;
}

function suspend(PID){
	for(var i in processes){
    if(processes[i].PID === PID){
      processes[i].suspend = true;
      print("Process " + PID + " suspended");
    }
  }
}

function resume(PID){
	for(var i in processes){
    if(processes[i].PID === PID){
      processes[i].suspend = false;
      print("Process " + PID + " resumed");
    }
  }
}

function killall(processName){
  var processNames = find(processName);
  for(var i in processNames){
    kill(processes[processNames[i]].PID);
  }
}

function getTotalFrametime(processesArray){
    var currentprocessesArray = processes;
    if(processesArray){
        currentprocessesArray = processesArray;
    }
  //Get total frametimes from all processes
  let totalFrameTime = 0;
  for(var i in currentprocessesArray){
    totalFrameTime += currentprocessesArray[i].frameTime;
  }
  return totalFrameTime;
}

function updateProcesses(processesArray){
	for(var i in processesArray){
    try{
      processesArray[i].update();
    }catch(error){
      console.error("Process " + processesArray[i].PID + " encountered an error!");
      console.error(error);
      kill(processesArray[i].PID);
      return error;
    }
	}
}

//Performance scheduler

function scheduleProcessesPerformance(processesArray){
  var prioritySum = 0;
  if(processesArray.length >= 1){
     prioritySum = processesArray[0].prioritySum;
  }
  for(var i in processesArray){
    processesArray[i].schedule(processesArray[0].prioritySum);
  }
}


//--------------------------------------------------------------------------------------------------------------------------------------

//Input management
var mouseArray = function(){
  this.x = 0;
  this.y = 0;
  this.vectorX = 0;
  this.vectorY = 0;
}
var keyboardArray = [];

function UpdateMouse(){
    mouseArray.vectorX = mouseArray.x-mouseX;
    mouseArray.vectorY = mouseArray.y-mouseY;
    mouseArray.x = mouseX;
    mouseArray.y = mouseY;
}

function UpdateKeyboard(){
  keyPressed = function(){
    keyboardArray[keyCode] = true;
  };
  keyReleased = function(){
    keyboardArray[keyCode] = false;
  };
}

//System suspend
function suspendSystem(processesArray){
  var currentProcessesArray = processes;
  if(processesArray){
    currentProcessesArray = processesArray;
  }
  for(var i in currentProcessesArray){
    currentProcessesArray[i].suspend = true;
  }
  print("System has been suspended.");
}

function resumeSystem(processesArray){
  var currentProcessesArray = processes;
  if(processesArray){
    currentProcessesArray = processesArray;
  }
  for(var i in currentProcessesArray){
    currentProcessesArray[i].suspend = false;
  }
  print("System has been resumed.");
}

function suspendResponseDaemon(){
  if(keyboardArray[192] && !this.suspended){
    suspendSystem();
    this.suspended = true;
  }
  if(this.suspended && !keyboardArray[192]){
    fill(0);
    rect(0,0,width,height);
    fill(255)
    text("Suspended",width/2-20,100);
    if(keyIsPressed){
      resumeSystem();
      this.suspended = false;
    }
  }
}



//Run the kernel
function setup(){
  frameRate(monitorFramerate);
  //Create process example:
  //createProcess(foo,priority);
}

function draw(){
  UpdateKeyboard();
  suspendResponseDaemon();
  UpdateMouse();
  scheduleProcessesPerformance(processes);
  updateProcesses(processes);
}

