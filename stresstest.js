var randomRange = 20;
function smallProcessorStressTest(){
  fill(255);
  // rect(mouseX+random(-randomRange,randomRange),mouseY+random(-randomRange,randomRange),20+random(-randomRange,randomRange),20+random(-randomRange,randomRange));
  rect(0,0,width,height);
  print("This process is stressing the system");
}

function largeProcessorStressTest(){
  for(var i = 0; i < 200; i++){
  var randomRangePlusI = randomRange+i;
  fill(255);
  rect(mouseX+random(-randomRangePlusI,randomRangePlusI),mouseY+random(-randomRangePlusI,randomRangePlusI),20+random(-randomRangePlusI,randomRangePlusI),20+random(-randomRangePlusI,randomRangePlusI));
  }
}

function fpsCounter(){
    noStroke();
    fill(140,140,140);
    rect(0,0,30,30);      
    stroke(0);
    fill(0);
    text(round(frameRate()),10,15);
    noStroke();
}

function recurseProcess(){
}

function jskernelStresstest(){
  print("Stressing process manager and scheduler");
  print("Stressing process manager pipeline by large amount");
  createProcess(function(){createProcess(function(){createProcess(recurseProcess)})});
}

function stress(){
  jskernelStresstest();
  createProcess(fpsCounter,0);
}
